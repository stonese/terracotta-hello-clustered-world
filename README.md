# Running Terracotta Hello Clustered World locally #

### Requirements: ###

1.  Java 1.6 or higher.
2.  Terracotta 3.7.5.  Download [here](http://www.terracotta.org/download/reflector.jsp?b=tcdistributions&i=terracotta-3.7.5.tar.gz)


### Steps ###

1. Extract the downloaded terracotta into directory %TC_DIR%
2. Download and extract this repository to directory %APP_DIR%
3. Change the currect working directory to %APP_DIR% using the command **cd %APP_DIR%**
4. Start the terracotta server using the command **%TC_DIR%/bin/start-tc-server.sh -f tc-config.xml**
5. Create the directory *build* under %APP_DIR% using the command **mkdir build**
6. Build the code using the command **javac -d build src/HelloClusteredWorld.java**
7. Start two instances of the application using the command **%TC_DIR%/platform/bin/dso-java.sh -cp build HelloClusteredWorld**
8. Monitor the clustered objects using the dev-console.sh **%TC_DIR%/bin/dev-console.s**
